Docker-Image: [Docker-Pihole](https://hub.docker.com/r/pihole/pihole) - Swiped the baseline config from piholes docker repo, but I've added my own tweaks, and [Tero Vepsäläinen](https://gitlab.com/webziz/pi-hole), who became the framework on my tweaks.


1. [Intro](https://gitlab.com/smiley.j0k3r/docker-pihole#intro)
2. [Choosing your Operating System](https://gitlab.com/smiley.j0k3r/docker-pihole#choosing-your-operating-system)
3. [Build](https://gitlab.com/smiley.j0k3r/docker-pihole#build)
4. [After OS Steps](https://gitlab.com/smiley.j0k3r/docker-pihole#after-os-steps)
5. [Docker Install](https://gitlab.com/smiley.j0k3r/docker-pihole#docker-install)
6. [PiHole Install](https://gitlab.com/smiley.j0k3r/docker-pihole#pihole-install)
7. [PiHole .yml (optional) modification](https://gitlab.com/smiley.j0k3r/docker-pihole#pihole-yml-optional-modification)
8. [Spinning up the Compose.yml file](https://gitlab.com/smiley.j0k3r/docker-pihole#spinning-up-the-composeyml-file)
9. [Note: Change the default password when PiHole is in a container](https://gitlab.com/smiley.j0k3r/docker-pihole#note-change-the-default-password-when-pihole-is-in-a-container)
10. [NOTE: Updating your Pihole container](https://gitlab.com/smiley.j0k3r/docker-pihole#note-updating-your-pihole-container)
11. [UPDATE - Using ENVironment files - 12/28/2021](https://gitlab.com/smiley.j0k3r/docker-pihole#update-using-environment-files-12282021)
12. [Conclusion](Conclusion)
## Intro

One of the projects that I have always wanted to do was taking the greatness of pihole, which was introduced to me awhile back from my good buddy Jas, and transition it to docker. I play with Docker and Kubernetes all day at work, and figured why not? Add it to my home lab and see what the results were.  Can I get the flexibility of docker, with the robustness of pihole.  Let's see

## Choosing your Operating System

Here is the tricky part that most folks will run through. Which OS to use? and more importantly, will we use the HOST OS/baremetal to manage the docker container, OR spin up a VM on our preferred virtualization host?  Well, for me, this was my initial planned setup, was going to stick to something simple and not too much thought, so went with Ubuntu Server. Why.......I love RaspbianOS, but sometimes it can be fickle and wanted something that would just spin up, and there are hundreds of articles/youtube channels going over the steps

## Build
- Host OS: [Proxmox *.*](https://www.proxmox.com/en/downloads)
- VM OS: [Ubuntu Server](https://ubuntu.com/download/server#downloads)
- Build: 
	- CPU: 1 core
	- Memory: 1 Gig
	- Storage: 20 Gig
	- Network: Bridge
		- please issue a static ip
		- also, make sure the default DNS is something external, like 8.8.8.8
	- Note: Minimal install

## After OS steps

After spinning up the VM, my first goal was to do updates to the OS.
```
sudo apt update -y && sudo apt upgrade -y
sudo reboot
```
we want to make sure everything is up-to-date and a clean reboot. Another to do is setup openssh-server so you can login remotely.  Now, some folks do this when you install the OS, but I prefer to do it afterward.
```
sudo apt install openssh-server
```
The next item we want to do is turn OFF 

 Now, this is where it gets REALLY fun and possibly dirty.  Now, the assumption is that snap is installed on your ubuntu instance already. Snap has been included with all ubuntu packages since [16.04 LTS](https://snapcraft.io/docs/installing-snap-on-ubuntu). But to validate.
```
snap --version
```
You SHOULD get the results like the following (the difference will be in the versions of snap)
```
******@pih:~$ snap --version
snap    2.53.4
snapd   2.53.4
series  16
ubuntu  21.10
kernel  5.13.0-22-generic
******@pih:~$ 
```
Yay! it looks like our version of snap is 2.53.4 (in this example, you may be different). Another cool display is, it shows our OS version. in this example, the machine is running Ubuntu Server 21.10.  Another way to get the version of your OS is
```
lsb_release -a
```
## Docker Install
Now that we know snap is installed and we are happy at this stage (no failures....hopefully for you), we can begin installing docker. Again, we will be using snap package manager to download and install docker. 
```
sudo snap install docker
```
Now, keep in mind, we are NOT done. In addition to installing docker, we will need to install docker-compose so that we can create .yml files for docker to run with. It's a process, but a simple one. The best instructions for that is to follow [Docker site: Install Compose on Linux systems](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)
Now, start up docker daemon.
 ```
 sudo systemctl start docker
 ```
 
 ## PiHole install 
 
Now that the docker daemon is started, we want to PULL the latest 
 ```
sudo docker pull pihole/pihole
 ```
 Now, keep in mind that pihole is a name resolution service, which listens on ports 53/TCP and 53/UDP. You know what else listens on that port, your machine by default to resolve names. Fun times. The daemon that does that on you your machine is called systemd-resolved.service. Our first step is to turn off that daemon.
```
sudo systemctl stop systemd-resolved.service
sudo systemctl disable systemd-resolved.service
```
OPTIONAL: Now don't worry, if things belly up and you want to enable systemd-resolve.service (provided pihole is not running), we just want to reenable and start the service. 
```
sudo systemctl enable systemd-resolved.service
sudo systemctl start systemd-resolved.service
```

## PiHole .yml (optional) modification

Hopefully, you've may have already done a git pull with this project. If so, please change your directory on your server to the location of the git location.
```
cd /somewhere/that/is/your/git/collection/
```
once you've gone there, please make sure the docker-compose file matches whatever you want it to be. the docker-compose.yml that we have here is a good starting point. But if you want some ideas to modify, let's open up the compose file.
```
nano docker-compose.yml
```
NOW, assuming this MAY be on a docker system that has multiple containers running, let's assume we need a close eye on the ports being used in the compose file. The default values I have here are:
```
    - ports:
      - 53:53/tcp
      - 53:53/udp
      - 67:67/udp
      - 85:80/tcp
```
this looks like a bunch of numbers, and your right. :) Breaking down the above, 
	- 85:80/tcp
	translates to
	- external port:internal port/protocol

Some of you may ask what are those ports. by order above they are:
53/tcp - DNS listening port via TCP
53/udp - DNS listening port via UDP
67/udp - DHCP listening port via UDP
85/TCP - WebGUI port. This can be any port on the external

So the item we want to pay attention to is the first number (85) , which is defined as an external port. That can be changed to whatever we want. We DON'T want to modify that second number (80), as inside the docker container, pihole is listening on port 80.  In this instance, if you went to your browser, and typed in your IP address and port 85 (example: http://192.168.1.x:85), you would be brought to the piholes webGUI. 
Now, think. are there any other containers you are running that are using port 85?  If so, simply change the port to something easy to remember OR whatever your company wants. Example:
```
    - ports:
      - 53:53/tcp
      - 53:53/udp
      - 67:67/udp
      - 8085:80/tcp
```
See that, the webGUI will listen on port 8085.

Next, we are going to look at the timezone on the compose. 
```
      TZ: America/Chicago
```
since I live in the midwest, my timezone is America/Chicago. If you live in the east coast, your entry would be TZ: America/NewYork.
Next, we will look at the volumes section.
```
    volumes:
      - /srv/ENTERLOCALORNASSSID/configs/pihole/etc-pihole/:/etc/pihole/
      - /srv/ENTERLOCALORNASSSID/configs/pihole/etc-dnsmasq.d/:/etc/dnsmasq.d/
```
I keep all my configs and on my home lab NAS. but if you are considering leaving local to the container, we would change it up a little.  
```
    volumes:
       - ./etc-pihole/:/etc/pihole/
       - ./etc-dnsmasq.d/:/etc/dnsmasq.d/
```
this indicates we are going local instead of using an external storage source. 

## Spinning up the Compose.yml file
ok, hopefully we have modified the .yml file to fit our needs. Now, provided we are in the folder with the compose file is located, we simply type the following:
```
sudo docker-compose up -d
```
if everything works well, we are ready for the races. 

## Note: Change the default password when PiHole is in a container

Whereas a traditional pihole instance running on a server we simply issue the command 
```
sudo pihole -a -p
```
we have to issue a command either inside the container itself or issue a docker exec (execute).  Lets try doing both.
```
docker exec -it [pihole_container_name] pihole -a -p
example: sudo docker exec -it pihole pihole -a -p
```
this will issue the docker execute inside the container called pihole to reset the password and you will be prompted to reset the webGUI password. 
The other way to change the password is to actually CLI/bash into the pihole container and change the password there. To do this, do the following:
```
sudo docker exec -it pihole bash
pihole -a -p
```
From this, we are actually inside the container stack and can issue the pihole string to change your webGUI. 


## NOTE: Updating your Pihole container

At some point, you will be brought to a point you'll want to update your pi-hole, FTL, or web interface. You know, the lovely blinky items at the bottom of the screen remind you that there are updates to be installed. Keep in mind that containers are meant to be spun up and down. This can be a lovely experience to learn from.  

Before we do anything, download the latest docker image of pihole
```
docker pull pihole/pihole
```
this will guarantee that we have the latest image before we do the follow-up actions. Keep in mind that pihole is (potentially) the primary DNS server. without it, no translation. So fetch that latest image. 
The next step is to stop the pihole container.
```
sudo docker stop pihole
```
now that pihole has stopped (remember, its our DNS server (name to IP resolution)), we want to remove the instance. Now, provided we have mapped our volumes in our compose, if we remove the instance, the volume/config should still be there. 
```
sudo docker rm pihole
```
now the pihole instance has been removed, but our logs, data should still be located in our NAS or the local machine. since we should be where the git repo is located at, we should be able to re-run the .yml file
```
******@******:~cd /somewhere/that/is/your/git/collection/
******@******:~/pihole$ sudo docker-compose up -d
Creating pihole ... done
```
and we are updated. 

## UPDATE - Using ENVironment files - 12/28/2021
Added the environment variable for storing your password in a separate file instead of inside the compose file. Please note that if you apply these changes to an existing container with a persistent volume, then the password environment file won't update by itself.  The process is as follows:
```
sudo docker stop pihole
sudo docker rm pihole
```
Now that we stopped and removed the container, we can still modify the existing folder layout, which is part of the next step. this is assuming we had the pihole container volume stored locally. 
```
cd /location/of/your/etc-pihole
example: /home/user/project/pihole/etc-pihole/
```
We want to look for setupVars.conf. Open it in sudo.  This is where the password was stored. We want to delete
```
sudo nano setupVars.conf
```
once opened, the file will look like the following
```
PIHOLE_DNS_1=8.8.8.8
PIHOLE_DNS_2=8.8.4.4 
BLOCKING_ENABLED=true
INSTALL_WEB_SERVER=true
INSTALL_WEB_INTERFACE=true
WEBPASSWORD='random_hash_values'  # we want to delete this line
PIHOLE_INTERFACE=eth0
QUERY_LOGGING=true
```
once you delete the WEBPASSWORD line, the file will look something like the following
```
PIHOLE_DNS_1=8.8.8.8
PIHOLE_DNS_2=8.8.4.4 
BLOCKING_ENABLED=true
INSTALL_WEB_SERVER=true
INSTALL_WEB_INTERFACE=true
PIHOLE_INTERFACE=eth0
QUERY_LOGGING=true
```
now cd .. back to the previous directory before /etc-pihole.  With the previous container gone, and the existing volume updated with a delete WEBPASSWORD, we can finally apply:
```
sudo docker-compose up -d
```
Test and the new password should be the same as whats in the environment file WEBPASSWORD.env. 

CAUTION: The minus effect of pihole is twofold.  
      - 1. The hash rate of the password is weak and easily [cracked](https://www.exploit-db.com/docs/49963)
            - the method above still doesn't protect you from the crack above. it just detaches the password to a file instead of inside your .yml file.
      - 2. You can utilize secrets, but to make that work with piHole, you'd need to enhance your pihole setup on either a Kubernetes cluster or Docker Swarm.
            - Also suggested to possibly use a 3rd party secrets manager like [vault](https://www.vaultproject.io/downloads)
            - I'll create a repo and readme.md on that after updating the other readme's

## Conclusion

oh man, talk about fun. There are a few CON's to mention.
1) The password is not very secure
2) if you use a docker manager like Portainer, it will pick up the WEBPASSWORD entry in your setupVars.conf, which would display the password in the webGUI when you look through your containers ENV entries. 
But otherwise, this was a fun experiment that will lead to a git repo for Swarm-PiHole and Kubernetes-PiHole

