
#!/bin/sh

if docker pull pihole/pihole| grep 'Image is up to date'; then
	echo '-> Image is up to date. Calm down'
else
	echo 'New image found. Uh oh'
	echo '-> Cleaning up old pihole'
	docker rm -f pihole
	echo '-> Running compose with the new image. Better open a browser to check if it works.'
	docker-compose up -d
fi
